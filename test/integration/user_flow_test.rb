require "test_helper"

class UserFlowTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:one)
    @book = books(:one)
  end

  test "user can view books and borrow" do
    get login_path
    assert_response :success

    post login_path, params: { email: @user.email, password: 'password' }
    assert_redirected_to root_path

    follow_redirect!
    assert_response :success

    get books_path
    assert_response :success

    get book_path(@book)
    assert_response :success

    post borrows_path, params: { borrow: { book_id: @book.id } }
    assert_redirected_to borrows_path

    follow_redirect!
    assert_response :success
  end
end

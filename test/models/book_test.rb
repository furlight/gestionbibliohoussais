require "test_helper"

class BookTest < ActiveSupport::TestCase
  def setup
    @book = Book.new(title: "Example Title", author: "Example Author", publish_year: 2023)
  end

  test "should be valid" do
    assert @book.valid?
  end

  test "title should be present" do
    @book.title = ""
    assert_not @book.valid?
  end

  test "author should be present" do
    @book.author = ""
    assert_not @book.valid?
  end

  test "publish_year should be present" do
    @book.publish_year = nil
    assert_not @book.valid?
  end
end

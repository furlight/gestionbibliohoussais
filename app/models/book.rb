class Book < ApplicationRecord
  has_many :borrows, dependent: :destroy
  has_many :users, through: :borrows

  validates :title, :author, :publish_year, presence: true

  def self.ransackable_attributes(auth_object = nil)
    ["author", "created_at", "id", "publish_year", "title", "updated_at"]
  end

  def self.ransackable_associations(auth_object = nil)
    ["borrows"]
  end
end

class User < ApplicationRecord
  has_secure_password

  has_many :borrows, dependent: :destroy
  has_many :books, through: :borrows

  validates :first_name, :last_name, :email, presence: true
  validates :email, uniqueness: true
end

class Borrow < ApplicationRecord
  belongs_to :user
  belongs_to :book

  validates :started_at, presence: true
  validate :book_available, on: :create

  def book_available
    if book.borrows.where(ended_at: nil).exists?
      errors.add(:book, "is already borrowed")
    end
  end
end

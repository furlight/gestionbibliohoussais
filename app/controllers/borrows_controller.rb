class BorrowsController < ApplicationController
  before_action :require_login

  def index
    @active_borrows = current_user.borrows.where(ended_at: nil).page(params[:page]).per(5)
    @borrow_history = current_user.borrows.where.not(ended_at: nil).order(ended_at: :desc).page(params[:history_page]).per(5)
  end

  def create
    @borrow = Borrow.new(borrow_params)
    @borrow.user = current_user
    @borrow.started_at = Time.current

    if @borrow.save
      redirect_to borrows_path, notice: t('notices.borrow_created')
    else
      redirect_to @borrow.book, alert: t('notices.cannot_borrow', errors: @borrow.errors.full_messages.join(", "))
    end
  end

  def destroy
    @borrow = Borrow.find(params[:id])
    @borrow.update(ended_at: Time.current)
    redirect_to borrows_path, notice: t('notices.borrow_returned')
  end

  private

  def borrow_params
    params.require(:borrow).permit(:book_id)
  end

  def require_login
    unless current_user
      redirect_to login_path, alert: t('alerts.must_be_logged_in')
    end
  end

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
end

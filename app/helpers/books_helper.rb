module BooksHelper
  def book_available?(book)
    book.borrows.where(ended_at: nil).empty?
  end
end
